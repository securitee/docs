# Enclaves
Our secure enclaves allow applications to run securely at the hardware lev-el, directed by the CPU itself. All data is encrypted in memory and decrypt-ed only when used inside the CPU. The data remains fully protected, even if the operating system (OS), hypervisor or root user is compromised. With the provision of secure enclaves, data can – for the first time – be protected across its entire life cycle: at rest, in motion and in use.

Our advanced secure enclaves are powered by Intel Software Guard Exten-sions (SGX). They offer further security through a process called ‘attestation’, which verifies that the CPU is genuine, and that the application is the valid one, i.e. that it has not been altered. The operation in secure enclaves provides users with full confidence that the code is running as in-tended and that the data is entirely protected.

![Enclave Process](assets/img/enclave_process.png)

## Running on Alpine Linux
Securitee PaaS runs Alpine Linux OS within its enclaves. In contrast to only running an SGX application or function, this feature allows users a unique new way of working with enclaves compared to other solutions. It enables a much wider field of application as it supports a broad usage of existing software .