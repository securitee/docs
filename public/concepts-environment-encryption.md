# Environment encryption

All Initial enclave code and data is measured by the CPU, allowing the user to ensure the confidentiality and integrity of environments. As the user is able to fully customize the environment, we require the loaded binaries and dependent libraries to be trustworthy. To verify and validate the envi-ronments, Securitee PaaS supports encryption and integrity protection at the block level by utilizing the standard Linux device mapper API.

<img align="right" src="assets/img/dm-crypt.png" height="400px">
Securitee PaaS utilizes ‘dm-crypt’, which is a transparent disk encryption subsystem in the Linux kernel. It is implemented as a device mapper tar-get and may be stacked on top of other device mapper transformations (Figure 11). Hence, it can encrypt whole disks, including removable devices, complete volumes and files. It is used as block device to support the full range of data at rest encryption. All device mapper modules receive their configuration via ‘DM-Tables’ - simple text files that specify how the device mapper should process accesses to the regions of the virtual disk. MADA-NA CORE handles and provides an easy way to utilize ‘dmsetup’, which reads these text files and passes their information to the kernel via ‘ioctl()’ calls.

The encryption software expects the key as a hex string of a fixed length. The module encrypts the data of the block device. Storing the key perma-nently in ‘DM-Table’-files would be equal to hanging a house key on the outside of the door. Therefore, it is necessary to create the key before each mounting.

Typing up to 32 hex digits on every startup is associated with a significant effort. Securitee PaaS provides an easy way to circumvent this inconven-ience by utilizing ‘cryptsetup’. The tool derives a cryptographic key from a (simpler) password and automatically passes it to the kernel.

Securitee PaaS enables the parameterization in two important areas: (i) The key generation and (ii) the encryption method. The former (i) deter-mines how a key is calculated from a user's password. Usually, it applies a hash algorithm, which gives the user the freedom to choose a password of any length. The hash always compresses the information to a certain num-ber of bytes. Concerning the encryption method (ii), there are two parame-ters to choose: The algorithm and the mode. Securitee PaaS passes these parameters to the kernel together with the derived key. Here, the ‘dm-crypt’ module coordinates the further process. For the purpose of encryp-tion, it accesses the proven Crypto-API. ‘Dm-crypt’, which can be used to:
•	Encrypt a complete file system covering additional scenarios, such as the LUKS extension, plain mode encryption and LVM.
•	Secure drive preparations with specific partitions and mounting points.
•	Allow encrypted swap partitions with and without suspend-to-disk support.
•	Allow booting and unlocking via the network and encrypt hook sup-porting multiple disk.
