# Connecting to enclave
Now that we have our application running in-enclave and verified via remote attesation that it is really running our configured environment we will take a look at how we are able to connect to our enclave.
This step can bee seen as optional as the application is already up and running and this step only referes to connecting to the enclave ( which might be needed to send / put data into the enclave).

In general there are 2 different ways

1. Connecting via portmapping
2. Connection via wireguard

## Connecting via portmapping

As we configured in the previous section, we defined a port that should be exposed on the securitee-paas. A random external port is assigned that allows diret access to the enclave which is useful for applications that need to be directly accessed.

In our scenario we exposed redis on the container port <code>56789</code> which is mapped to the external port <code>31609</code>.
We can now interact with our database using the external port and the ipaddress / dnsname that is assigned to your securitee-paas. As we are using redis, we are using the [redis-cli](https://redis.io/topics/rediscli) to interact with our database.

We will use the -h (host) and -p (port) parameter to connect to our database and use the ping command to verify connectivity and set two values and get them afterwards to ensure everything is working as expected

<br><br>

 ![redis-cli](assets/img/ui_rediscli.gif)





## Connecting via wireguard

If you don't want publicly expose a port of your enclave but still to want / need to interact with your application you can use the built-in wireguard tunnel. To be later on able to connect from your client via wireguard you need to provide your client's wireguard public key when starting your enclave. You can do this either via the graphical interface or via the [securitee-paas-cli]() tool.
Via the securitee-paas-cli tool you are not only able to attestate remote running enclaves but also you are to transmit the run-configuration.
So let's start from the point where you have started up the enclave verified the remote attestation via tool.

### add enclaves public key to your environment file
```
$ echo ENCLAVE_WG_PUB_KEY=U6eJFenuwhClipTAWoPyNbqlMLTsy66LfAbhmJYV5mA= > ./env
```

### start enclave via remote run config
Now that the enclave has been successfully attested and we have been able to retrieve the enclaves wireguard public key. With this information we can now send the remote run config, that is, the actual command that should be executed within the enclave. To do so several other things are used within the "start" script.

| | |
|-|-|
|WG_PRIV_KEY env variable | the wireguard private key configured to be the only key the enclave will talk to |
|WIREGUARD_PORT env variable| port on which the enclaves wireguard service is listening |
|ROOTHASH env variable| the roothash of the enclaves disk image, used to verify what is being executed|
|remote-config.json file | the enclave run config, including its run command and and disk configuration |

```
$ docker run --rm -it --env-file ./env --cap-add NET_ADMIN securitee-redis-sgx-example start
Connecting to 192.168.99.1:56001... done.
[    SGX-LKL   ] Request successful.
```
> *NOTE:* the container needs `CAP_NET_ADMIN` capabilities in order to configure a wireguard device. Also this will only work on newer kernels which are compiled with wireguard support by default.

### connect to redis via wireguard
Connecting to redis via wireguard you can be sure that you are actually communicating with redis inside the enclave as the enclaves wireguard key and hence the wireguard tunnel has previously been successfully attested to be belonging to the enclave
```
$ docker run --rm -it --env-file ./env --cap-add NET_ADMIN securitee-redis-sgx-example redis-cli -h 192.168.99.1 -p 56789
```