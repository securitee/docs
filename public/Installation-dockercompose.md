# Docker compose

The docker compose is starting up 3 different containers and can be used as standalone version supporting all currently implemented features.
By using docker-compose 3 different containers are started.  [securitee-paas](https://gitlab.com/securitee/securitee-paas/container_registry) ,  [ipfs](https://gitlab.com/securitee/securitee-paas/container_registry) and the I [Intel AESMD Service](https://gitlab.com/securitee/sgx-aesmd/container_registry) 

## Requirements
Make sure docker is installed with a recent version

```
# these versions have been successfully tested:
docker --version
Docker version 19.03.2, build 6a30dfc
```

All versions are deployed to the gitlab [docker repository](https://gitlab.com/securitee/securitee-paas/container_registry) 
Login to gitlab docker registry to have access to the containers
```
docker login registry.gitlab.com
```

In addition you need to have git installed.

```
git --version
```





##  Usage
---
### pull the latest version

Create a new directory to which the securitee-paas application will be cloned. Within this directory run the below command to checkout the current master branch.

```
git clone https://gitlab.com/securitee/securitee-paas.git
```


### clone stable version
To run the latest tested and deployed version please use the production tag
```
git clone https://gitlab.com/securitee/securitee-paas.git
git checkout production
```

### Change to the newly created folder
```
cd securitee-paas
```

### Start the container
To run the checked out version use the below commands within the directory
```
docker-compose up
```

If you want to run the application in the background (you are able to close your commandline and the application keeps running) use the -d (detached) flag
```
docker-compose up -d
```

###  Shutdown the running version
To shutdown the checked out version use the below commands within the directory
```
docker-compose down
```
###  Delete the application
To delete the application use the below command. For the application multiple volumes (containing environments and config) might be created which will be deleted.
```
docker-compose rm
```
After you deleted the docker-related volumes and containers you are free to delete the folder


###  Updating the apllication
Open a terminal within the securitee-paas folder and run
```
git pull
```



### Using a docker compose hosted on another host

```
# start docker compose setup
docker-compose up

# port forward http port to your laptop
ssh -fNT  -L 9291:localhost:9291 user@remotehost

# port forward ipfs port to your laptop
ssh -fNT  -L 5001:localhost:5001 user@remotehost
```
