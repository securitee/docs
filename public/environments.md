# Environments
To run applications within enclaves, they need to be provided as an envi-ronment, which is essentially a disk image. In more detail, a disk image (in computing) is a computer file containing the contents and structure of a disk volume of an entire data storage or of a device (such as a hard disk drive, tape drive, optical disc or USB flash device).

A disk image is usually produced by creating a sector-by-sector copy of the source medium, thereby perfectly replicating the structure and contents of a storage device independent of the file system.

