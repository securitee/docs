#  Installation

## Disclaimer
The securitee-paas and its implementations are still in heavy development. This means that there may be problems in our protocols, or there may be mistakes in our implementations. We take security vulnerabilities very seriously. If you discover a security issue, please bring it to our attention right away! If you find a vulnerability that may affect live deployments -- for example, by exposing a remote execution exploit -- please send your report privately to info@securitee.tech. Please DO NOT file a public issue. If the issue is a protocol weakness that cannot be immediately exploited or something not yet deployed, just discuss it openly.


## Available modes
The application can be run in 3 different modes.

1. [Standalone](https://gitlab.com/securitee/securitee-paas/-/edit/master/README.md#standalone])
  
  asdasdasdasdasd
2. [Docker Compose](https://gitlab.com/securitee/securitee-paas/-/edit/master/README.md#docker-compose)
3. [Kubernetes Deployment ](https://gitlab.com/securitee/securitee-paas/-/edit/master/README.md#kubernetes-deployment)
