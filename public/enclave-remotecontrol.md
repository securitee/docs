# Enclave Remote Coontrol
Securitee PaaSs enclave attestation response also includes the public WireGuard key of the enclave, enabling a client to establish a secure con-nection. This is not only used to communicate securely with an application, but also to provide enclave secrets, such as disk encryption keys and the application.
In release mode, an application configuration has to be provided remotely (as it includes sensitive disk encryption keys, application arguments, and environment variables that need to be protected). The application configu-ration includes:
•	Disk encryption keys and root hashes
•	Additional WireGuard peers
•	The path to the executable to run
•	Application command line arguments
•	Environment variables exposed to the application
 
