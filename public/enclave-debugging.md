# Enclave debugging & profiling
<img align="left" src="assets/img/debugging.png" height="800px">

## GDB plugin

Securitee PaaS can be used with ‘gdb’ to trace both the kernel part and running the applications (Figure 17:
•	Breakpoints, watchpoints, stack traces
•	Dynamically loads required symbols
•	Supports software simulation and hardware SGX mode



## Perfsupport 
Perf is a profiler tool for Linux 2.6+ based systems that abstracts away CPU hardware differences in Linux performance measurements and presents a simple commandline interface. 

Support for profiling with ‘perf’ is currently limited to simulation mode. By default, no symbols of the application or its dependencies are available to perf due to the in-enclave linking/ loading and the fact that ‘perf’ has no access to the executables and libraries within the disk image. By mounting the disk image and setting to the path of the mount point, ‘perf’ can be enabled to resolve these symbols. Securitee PaaS passes the required enclave symbols to perf.
