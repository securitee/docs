# Prerequisites
The securitee-paas can be run both in hardware mode (when it requires an Intel SGX compatible CPU) and in software simulation mode (when it runs on any Intel CPU without hardware security guarantees). If you don't want to run the securitee-paas within secure hardware mode you are not required to meet the prerequesits.

## Hardware
Ensure that you have the following required hardware:

6th Generation Intel(R) Core(TM) Processor or newer

## Operating System
Ensure that you have one of the following required operating systems

Ubuntu* 16.04.3 LTS Desktop 64bits

Ubuntu* 16.04.3 LTS Server 64bits

Ubuntu* 18.04 LTS Desktop 64bits

Ubuntu* 18.04 LTS Server 64bits

## Software

##### Enable support for Thread-Local Storage (TLS) via sgx-lkl
```
git clone  
cd sgx-lkl/tools/kmod-set-fsgsbase/
make set-cr4-fsgsbase
```
