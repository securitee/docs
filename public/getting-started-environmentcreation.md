# Create your first environment  

To run applications within enclaves they need to be provided as enviroment which is an disk image. You can think of environment as bootable device that is loaded upon initialization of the enclave. The securitee-paass takes care of the complete creation process and handling so you are able to create your environments via an easy to handle interface.

[A](https://en.wikipedia.org/wiki/A "https://en.wikipedia.org/wiki/A") **disk image**, in computing, is a [computer file](https://en.wikipedia.org/wiki/Computer_file "https://en.wikipedia.org/wiki/Computer_file") containing the contents and structure of a disk [volume](https://en.wikipedia.org/wiki/Volume_(computing) "https://en.wikipedia.org/wiki/Volume_(computing)") or of an entire [data storage](https://en.wikipedia.org/wiki/Data_storage "https://en.wikipedia.org/wiki/Data_storage") device, such as a [hard disk drive](https://en.wikipedia.org/wiki/Hard_disk_drive "https://en.wikipedia.org/wiki/Hard_disk_drive"), [tape drive](https://en.wikipedia.org/wiki/Tape_drive "https://en.wikipedia.org/wiki/Tape_drive"), [floppy disk](https://en.wikipedia.org/wiki/Floppy_disk "https://en.wikipedia.org/wiki/Floppy_disk"), [optical disc](https://en.wikipedia.org/wiki/Optical_disc "https://en.wikipedia.org/wiki/Optical_disc"), or [USB flash drive](https://en.wikipedia.org/wiki/USB_flash_drive "https://en.wikipedia.org/wiki/USB_flash_drive"). A disk image is usually made by creating a [sector](https://en.wikipedia.org/wiki/Disk_sector "https://en.wikipedia.org/wiki/Disk_sector")\-by-sector copy of the source medium, thereby perfectly replicating the structure and contents of a storage device independent of the [file system](https://en.wikipedia.org/wiki/File_system "https://en.wikipedia.org/wiki/File_system"). Depending on the disk image format, a disk image may span one or more computer files. We are using it as virtual disk drive space to be used by [SystemVirtualization](https://wiki.debian.org/SystemVirtualization "https://wiki.debian.org/SystemVirtualization"). As our current implementation is built on top of musl, applications are expected to be dynamically linked against musl. musl and glibc are not fully binary-compatible. Applications linked against glibc are therefore not guaranteed to work with the securitee-paas.


Open the environments page which is the main page to handle and manage your environments.
<br><br>

 ![Environment page](assets/img/ui_environmentsinitial.png)
  

## Add environment
On the top of the environments you can see a table listing all available environments. Per default the securitee-paas comes with the securitee-ape (analysis processing entity) pre-installed.
We will now not use the securitee-ape environment but create a fresh new one. To do so please click the "Add environment" below the table which will open up a dialog.
<br><br>

 ![Environment add dialog](assets/img/ui_environmentadddialog.png)

The securitee-paas currently provides 3 different way to add new environments to your securitee-paas

**Create new environment** - Enables you to create a new disk image, install alpine packages and add custom files

**Convert docker images** - Enables you to convert existing docker images

**Add existing environment via IPFSHash** - Enables you to download existing environments that have been published by you or other users

In this getting starting guide we are utilizing the simplest way to create environments and run will use prebuilt binaries for Alpine Linux which uses musl as its C standard library. So please choose option 1 **Create new environment** . You'll be redirected to a new page that enables you to configure your new environment.

## Configure new environment

### Basic information

The environment creation is starting with basic information that needs to be entered.
First of all you need to a name for your new environment. The environment name is used as unique identifier for your environment on your securitee-paas and can be freely chosen.

At the next step you are required to define the size of your image. As this image is created used as block device you are required to define a size that will fit in all the data that you'll add. The size is specified in megabyte identified by the trailing M in the textfield. For our initial case we will need 100 megabyte so please enter <code>100M</code>.
<br><br>

 ![Environment add basic information](assets/img/ui_environmentadd_basic.png)

As last step for the basic information you'll see an embedded markdown editor. The editor can be used to create a readme which is automatically bundled with the environment. This is extremly useful if you want to share your environments with other users later on or upload your environment to the [SECURITEE environment marketplace](). For this tutorial you can skip the customisation of the readme.

### Package configuration
The securitee-paas provides a graphical interface to install alpine packages into your environment.
<br><br>

 ![Alpine package config](assets/img/ui_environmentpackageconfig.png)

Alpine packages are digitally signed tar.gz archives containing programs, configuration files, and dependency metadata. They have the extension .apk, and are often called "a-packs". If you are familiar with alpine linux or other package managers it's nothing new for you. It let's you install precompiled binaries with ease of use. These alpine packages can come from two different sources: 

*   **About the main packages**: Main packages are the Alpine package software that have direct support and updates from the Alpine core and main team, also have official special documentation. Are always available for all releases and will have almost substitutions if some are not continued from upstream. Commonly those packages are selected due their responsibility and stability respect upstream availability.
    
*   **About the contribution ones**: User package contribution repositories are those made by users in team with the official developers and well near integrated to the Alpine packages. Those have supported by those user contributions and could end if the user also ends respect with Alpine work, by example could not have substitution in next release due lack of support by upstream author.
 

Only the main repository is enabled per default, so apk will not include packages from the other repositories. Besides using the embedded search you can also visit [https://pkgs.alpinelinux.org/packages](https://pkgs.alpinelinux.org/packages "https://pkgs.alpinelinux.org/packages") directly and browse for existing applications. For more information on alpine packages feel free to read the [Alpine newbie apk packages](https://wiki.alpinelinux.org/wiki/Alpine_newbie_apk_packages "https://wiki.alpinelinux.org/wiki/Alpine_newbie_apk_packages") page.

In this getting started guid we will install the redis package to be able to run redis in-enclave. As a user you able to install as much packages you want ( as long as they fit into the specified image size). To showcase that we will also add python. We are just searching for the package names on the right page which will show relevant packages and let us add packages by using the icon next to it. The left column shows all packages that will be install.
<br><br>

 ![Configure alpine packages](assets/img/ui_environmentpackages.gif)

 ### Optional run configuration
 <br><br>

 ![Alpine package config](assets/img/ui_environmentrunconfig.png)
 As last step in the configuration you can optionally create an default run configuration. A run configuration specifies which executable should be executed when the enclave boots up using this environment. While you'll be asked for the run configuration when you startup the enclave and you are also able to submit the run configuration after the startup via [remote control](), it makes sense to enter a default run configuration as it will be proposed as default everytime you start an enclave. This default run configuration will also be packaged with the environment if you decide to publish your environment so other users have an easier startup process. **Important: As the default run configuration will also be published when you decide to publish your environment you shouldn't enter any secrets. If any secrets or credentials are required as paramter please use the [remote control funcitonality]()** 

 In our example we will want to run redis datbase which is accessible via the internet. So please enter as executable path <code>/usr/bin/redis-server</code> as executable path and enter <code>--port 56789 --protected-mode no</code> as application paramters. This will run redis as in unprotected listening on port 56789. **Important: We are using the unprotected mode only to showcase the access from remote. You should under no circumstances use this for a production database**

 If you have entered the executable path and parameters hit the "add environment" button at the bottom which will save your configuration and redirect you back to the environments page.
 You should now see your newly created environment in the table.
  <br><br>

 ![Environment created](assets/img/ui_environmentscreated.png)

## Add custom files
As you can see your newly created environment doesn't provide an disk image hash, that is because the image hasn't been built yet. Before the image get's build you are able to upload additional files which will be packaged into environment.
The securitee-paas allows users to copy files from the host to the disk image in case they need to add extra data or files in the disk image. By adding additional files, a key store or script can be embedded into the environment which than can be used in-enclave during runtime.

Just to showcase this functionality we are creating a new (python) file helloword.py with the following content on our desktop
```python
print( "Hello (Enclave) World");
```
Now click on the burger menu in the last column of the environment row and choose <code>Upload files to {Environmentname}</code>. A new dialog will open up with a button that opens a fileselector and a textfield that allows you to specify the path to where this file should be uploaded. Click the button, select your file and press upload at the bottom of the dialog to upload the helloworld.py to the root (<code>/</code>) of the disk image.
<br><br>

 ![Upload custom files](assets/img/ui_fileupload.gif)


Afterwards you should see the file with it's full path next to the burgermenu within the row of the environment

## Build an environment
As last step in the environment creation the environment needs to be build. As we previously create the complete config and specifed files that should be added, all you need to do now is to click on the burger menu and choose <code>Build environment</code>. The build process will be initialized which installs all packages (and their dependencies) and prepares the environment. Depending on your configuration this could take a couple of seconds.

After you the environment has been built you'll see the build log. During the build process a hash of the environment is created which you'll see in the table. The hash will later on be used to validate the integrity of the environment. If the environment should change in anyway, the hash would change what the system would identify upon usage.
<br><br>

 ![Build environment](assets/img/ui_buildenvironment.gif)


That's it!
We created our first environment containing redis and python2 package and have also added custom helloworld.py script.


