# IPFS

![ipfs](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/IPFS_logo.png/800px-IPFS_logo.png)

IPFS is a distributed system for storing and accessing files, websites, applications, and data. 

IPFS is Making it possible to download a file from many locations that aren’t managed by one organization…

*   **Supports a resilient internet.** If someone attacks Wikipedia’s web servers or an engineer at Wikipedia makes a big mistake that causes their servers to catch fire, you can still get the same webpages from somewhere else.
    
*   **Makes it harder to censor content.** Because files on IPFS can come from many places, it’s harder for anyone (whether they’re states, corporations, or someone else) to block things. In 2017, Turkey blocked Wikipedia and Spain blocked access to sites related to the Catalonian independence movement. We hope IPFS can help provide ways to circumvent actions like these when they happen.
    
*   **Can speed up the web when you’re far away or disconnected.** If you can retrieve a file from someone nearby instead of hundreds or thousands of miles away, you can often get it faster. This is especially valuable if your community is networked locally, but doesn’t have a good connection to the wider internet. (Well-funded organizations with technical expertise do this today by using multiple data centers or CDNs — [content distribution networks](https://en.wikipedia.org/wiki/Content_delivery_network "https://en.wikipedia.org/wiki/Content_delivery_network"). IPFS hopes to make this possible for everyone.)
    

That last point is actually where IPFS gets its name: **Inter-Planetary File System**. THey'restriving to build a system that works across places as disconnected or as far apart as planets. While that’s an idealistic goal, it keeps us working and thinking hard, and most everything we create in pursuit of that goal is also useful here at home.

The IPFS project seeks to evolve the infrastructure of the Internet and the Web, with many things we've learned from successful systems, like [Git](https://git-scm.com/ "https://git-scm.com/"), [BitTorrent](http://bittorrent.org/ "http://bittorrent.org/"), [Kademlia](https://en.wikipedia.org/wiki/Kademlia "https://en.wikipedia.org/wiki/Kademlia"), [Bitcoin](https://bitcoin.org/ "https://bitcoin.org/"), and many, many more. This is the sort of thing that would have come out of ARPA/DARPA, IETF, or Bell Labs in another age. IPFS is a free, open-source project with thousands of contributors.

IPFS ([the InterPlanetary File System](https://docs-beta.ipfs.io/concepts/what-is-ipfs/ "https://docs-beta.ipfs.io/concepts/what-is-ipfs/")) is a hypermedia distribution protocol addressed by content and identities. It enables the creation of completely distributed applications, and in doing so aims to make the web faster, safer, and more open.

IPFS is a distributed file system that seeks to connect all computing devices with the same system of files. In some ways, this is similar to the original aims of the Web, but IPFS is actually more similar to a single BitTorrent swarm exchanging Git objects. You can read more about its origins in the paper [IPFS - Content Addressed, Versioned, P2P File System](https://github.com/ipfs/ipfs/blob/master/papers/ipfs-cap2pfs/ipfs-p2p-file-system.pdf?raw=true "https://github.com/ipfs/ipfs/blob/master/papers/ipfs-cap2pfs/ipfs-p2p-file-system.pdf?raw=true").

#### Starting up IPFS Node


