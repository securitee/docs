# securitee-paas
The securitee-paas allows users to create and manage environments which can be run within enclaves and remotely attestated by 3rd Parties and the Intel Attestation Service. Created environments can be shared and exchanged with other users and peers via IPFS and users are able to provide their computing power to run environments in substitution for other users based on the securitee protocol.


It allows existing unmodified Linux binaries inside of Intel SGX enclaves to run while providing the necessary system support for complex applications (e.g., TensorFlow, PyTorch, OpenVINO) and for programming language runtimes (e.g., Python, the DotNet CLR, JVM). All of these applications can be run in SGX enclaves without the need for modifications or reliance on the untrusted host OS.

We support in-enclave user-level threading, signal handling as well as file and network I/O. System calls are handled within the enclave by the Linux Kernel Library, while the host is used only for access to I/O resources.

## Concept
Our secure enclaves allow applications to run securely at the hardware lev-el, directed by the CPU itself. All data is encrypted in memory and decrypt-ed only when used inside the CPU. The data remains fully protected, even if the operating system (OS), hypervisor or root user is compromised. With the provision of secure enclaves, data can – for the first time – be protected across its entire life cycle: at rest, in motion and in use.

Our advanced secure enclaves are powered by Intel Software Guard Exten-sions (SGX). They offer further security through a process called ‘attestation’, which verifies that the CPU is genuine, and that the application is the valid one, i.e. that it has not been altered. The operation in secure enclaves provides users with full confidence that the code is running as in-tended and that the data is entirely protected.

![Enclave Process](assets/img/enclave_process.png)

The securitee-paas runs Alpine Linux OS within its enclaves. In contrast to only running an SGX application or function, this feature allows you a unique new way of working with enclaves compared to other solutions. It enables a much wider field of application as it supports a broad usage of existing software 

![System Support](assets/img/systemsupport.png)

Alpine Linux is a special distribution primarily made for the deployment of applications on the overall Linux distribution and is a rising competitor for Ubuntu. Alpine Linux combines security with simplicity and resource effi-ciency. It is designed to run directly from RAM. The latest version of Alpine Linux is approximately 4MB, which is conveniently extremely small.

![Binary Support](assets/img/binarysupport.png)

As part of the creation process, additional software packages can be in-stalled to add support for managed language runtimes or even for entire applications. Software packages for Alpine Linux are digitally signed ‘tar.gz’ archives containing programs, configuration files and dependency metadata. They have the extension ‘apk’ and are often called ‘a-packs’. As a user, you can install these packages when creating a new environment, thereby satisfying the dependencies that your applications need.

![Alpine package repository](assets/img/apkrepository.png)

The programs and software installed to Linux Alpine can come from two sources: custom repositories and original upstream sources which are compiled in the traditional Unix-like way 

![Language runtimes](assets/img/languageruntimes.png)

The runtime languages above are just a few examples of the many that can be run inside our enclaves'

## Disclaimer
The securitee-paas and its implementations are still in heavy development. This means that there may be problems in our protocols, or there may be mistakes in our implementations. We take security vulnerabilities very seriously. If you discover a security issue, please bring it to our attention right away! If you find a vulnerability that may affect live deployments -- for example, by exposing a remote execution exploit -- please send your report privately to info@securitee.tech. Please DO NOT file a public issue. If the issue is a protocol weakness that cannot be immediately exploited or something not yet deployed, just discuss it openly.

##### Test
