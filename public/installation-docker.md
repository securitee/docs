# Docker

As the standalone version is missing important resources, this mode should only be used for debugging purposes.

## Requirements
Make sure docker is installed with a recent version

```
# these versions have been successfully tested:
docker --version
Docker version 19.03.2, build 6a30dfc
```

All versions are deployed to the gitlab [docker repository](https://gitlab.com/securitee/securitee-paas/container_registry) 
Login to gitlab docker registry to have access to the containers
```
docker login registry.gitlab.com
```

##  Usage
---
### pull and run latest version


Because the latest tag always refers to the master branch, it includes the most recent changes and should be seen as integration stage.

```
 docker pull registry.gitlab.com/securitee/securitee-paas:latest
 docker run -p 9291:9291 -p 5001:5001 -p 4001:4001 -p 8080:8080 -it --privileged=true registry.gitlab.com/securitee/securitee-paas:master
```


### pull and run stable version
To run the latest tested and deployed version please use the production tag
```
 docker pull registry.gitlab.com/securitee/securitee-paas:production
 docker run -p 9291:9291 -p 5001:5001 -p 4001:4001 -p 8080:8080 -it --privileged=true registry.gitlab.com/securitee/securitee-paas:production
```

###### -p 9291:9291
To expose a container’s internal port, an operator can start the container with the -P or -p flag. The exposed port is accessible on the host and the ports are available to any client that can reach the host. The port number inside the container (where the service listens) does not need to match the port number exposed on the outside of the container (where clients connect). For example, inside the container an HTTP service is always listening on port 9291. At runtime, the port might be bound to 9292 on the host to run a secound node on the same host by using 
```
 docker run -p 9292:9291 --rm -it --privileged=true registry.gitlab.com/securitee/securitee-paas:latest
```

###### --rm
By default a container’s file system persists even after the container exits. This makes debugging a lot easier (since you can inspect the final state) and you retain all your data by default. But if you are running short-term foreground processes, these container file systems can really pile up. If instead you’d like Docker to automatically clean up the container and remove the file system when the container exits, you can add the --rm flag:

```
--rm=false: Automatically remove the container when it exits
```
###### -it --privileged=true
By default, Docker containers are “unprivileged” and cannot, for example, run a Docker daemon inside a Docker container. This is because by default a container is not allowed to access any devices, but a “privileged” container is given access to all devices (see the documentation on cgroups devices).

When the operator executes docker run --privileged, Docker will enable access to all devices on the host as well as set some configuration in AppArmor or SELinux to allow the container nearly all the same access to the host as processes running outside containers on the host. Additional information about running with --privileged is available on the Docker Blog.


###### Cleanup

As the application is binding to a specified port, no multiple instances can be run without manual setting another port.

If you receive an error message like
```
Error response from daemon: ...  Bind for 0.0.0.0:9291 failed: port is already allocated.
```
it means that there is already an container running on the defined port. To list all running containers execute docker ps

```
docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
f3ea2e2289d6        9b57d67bd1a5        "/entrypoint.sh /bin…"   14 minutes ago      Up 14 minutes       0.0.0.0:9291->9291/tcp   epic_mclean
```

Now copy the relating NAME and execute docker kill followed by the name of the container

```
docker kill epic_mclean
```
Re-run docker ps to make sure the container has been killed
```
 docker ps
 CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
```
