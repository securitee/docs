# Wireguard

![wireguard](https://www.wireguard.com/img/wireguard.svg)

A lightweight WireGuard endpoint is embedded into every enclave allow-ing users to make secure connections to the enclave and without having to rely on the untrusted OS or its applications.

WireGuard is an extremely simple, yet, fast and modern VPN that utiliz-es state-of-the-art cryptography. For example, it has better performance statistics than OpenVPN. WireGuard is designed as a general-purpose VPN for running on embedded interfaces and super computers alike. At the same time, it is a great fit for under a lot of different circumstances. Initially released for the Linux kernel, it is now available cross-platform (Windows, macOS, BSD, iOS, Android) and widely deployable. It is currently under heavy development, but it might already be the most secure and easiest to use VPN solution in the industry.

WireGuard uses state-of-the-art cryptography, such as the Noise protocol framework, Curve25519, ChaCha20, Ploy1305, BLAKE2, SipHash24, and se-cure trusted constructions. It makes conservative and reasonable choices and has been reviewed by cryptographers.
