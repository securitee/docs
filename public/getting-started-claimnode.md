# Claim your securitee-paas

After you have acquired or installed your securitee-paas you are able to access the webfront by opening the browser and going to the URL we've sent you, or that is shown to you when you deployed.
To claim your securitee-paas (which makes it only accessible for you) you'll have to enter your securitee-credentials. If you don't have an account yet please visit the [SECURITEE Core Controlpanel](https://http://core.securitee.tech/register) in order to create one.

![securitee-paas login](assets/img/login.png)

Upon succesful authentication your hashed credentials are stored on the madan-node filesystem and basic-authentication will be enabled. For more detailed information on the authentication please have a look at the[Authentication & key management](https://http://core.securitee.tech/register) page.

You are now being redirected to the homepage showing you basic information about your node and license status.

![securitee-paas login](assets/img/ui_homepage.png)

