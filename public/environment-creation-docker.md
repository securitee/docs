Creating environments from docker images
========================================
Docker is a convenient tool for building, shipping and running applications. Many companies have already moved their legacy applications to Docker. Securitee PaaS allows the conversion of Docker containers into runnable environments (Figure 10). This enables users to utilize secure enclaves without manual app modifications or recompilations.

![Docker conversion](assets/img/dockerconversion.png)

This article describes how to convert existing docker-images into environments (disk images)

Currently the UI only supports the conversion of docker images that can be pulled without authentication from dockerhub.

The execution of applications within environments are limited to applications that have been compile with musl. So in general it’s a good idea to use docker images with alpine as base image

[https://hub.docker.com/search?q=alpine-&type=image](https://hub.docker.com/search?q=alpine-&type=image "https://hub.docker.com/search?q=alpine-&type=image") 

Instructions
------------

1.  Login to your securitee-paas
    
2.  Go to the environments page by clicking on “environments” on the top bar
    
3.  Click the “Add Environment” Button below the environments table which opens up a dialog
    
4.  Click on “Convert docker image”
    
5.  The docker conversion dialog expects an repository name followed by “/” and the image name ( like in this example jfwenisch/alpine-tor ). The name to be entered can be found at the top right side when visiting [dockerhub](https://hub.docker.com/ "https://hub.docker.com/")
    
    
6.  Enter the wanted name and press the download button right next to it
    
    
7.  The image is now being pulled, and afterwards it’s ID is being used to start the conversion. Depending on your securitee-paas internet connection , the size of the docker-images and available computing resources this might take a couple of minutes.
    
8.  The result should look like the image below. Afterwards you can go back to the environments page to start your new environment. The environment is automatically named after the used repository and image ( replacing all “/” with “-” )
    
    

Created environments might be 50% bigger in filesize than the origin docker images
