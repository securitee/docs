# Verify attestation

Let's get paranoid and think about the visual output that we see on processing page is fake we currently got serveral options


## Check the Intel developers analytics dashboard

As first option we can refer to Intel developers analytics dashboard. As we have set up a dedicated subscriptions to track the remote attesation you are now able to go the [Intel developers analytics dashboard]( https://api.portal.trustedservices.intel.com/Developer/Analytics) and verify if it really have been sent to Intel and verified by them.

<br><br>

 ![Intel analytics dashboard](assets/img/ui_intelanalyticsdashboard.png)


Set the filter to today check for successful ( or failed ) responses. In our case we can see that the call has been succsfully answered


## Use the securitee-attesation-verify application 
As the enclave provides a dedicated attestation service which provides quote that can be verified you can verify the attesation from a remote client.
We provide you with an open source docker container so that you are able to verify that information from a remote client.

To be able to run verify the attesation, you need to clone this [repo]() and create a new file "env" in the repositories directory

### build container image
First of all create the app docker image that will be run. The used executables are open source and the source code can be found [here](https://github.com/SECURITEE/sgx-lkl) . Feel free to check the code and compile them yourself.
```
docker build -t securitee-redis-sgx-example .
```

### run remote attestation
You are now going to remotely attest an enclave which is running on SGX enabled hardware and was provided to you to test securitee-paas' capabilities. Having a look at the "attest" script in the source directory, you will notice several things necessary to attest the enclave. Please specify the following variables within the env efile:

| | |
|-|-|
| ENCLAVE_DOMAIN env variable | domain pointing to server running the enclave |
| ATTESTATION_PORT env variable | port on which the enclaves attestation routine is listening |
| IAS_SPID env variable | a IAS Service Provider ID we have provided |
| IAS_SKEY env variable | a IAS Signing Key we have provided |
| MRENCLAVE_QUOTE env variable | Enclave identity |
| MRSIGNER_QUOTE env variable | Signing Enclave identity |


After you created the file you can run the remote attesation against the endpoint by running: 
```
$ docker run --rm -it --env-file ./env securitee-redis-sgx-example attest
Connecting to cluster.securitee.tech:32606... done.
Request successful.
[    SGX-LKL   ] Sending IAS request...
[    SGX-LKL   ] Intel Attestation Service Response:
[    SGX-LKL   ]  Quote status: GROUP_OUT_OF_DATE
[    SGX-LKL   ]  EPID group flags: 0x4
[    SGX-LKL   ]  TCB evaluation flags: 0x0f
[    SGX-LKL   ]  PSE evaluation flags: 0x00
[    SGX-LKL   ] Warning: Quote status: GROUP_OUT_OF_DATE (Platform software/firmware is out of date)
[    SGX-LKL   ] Quote measurements:
[    SGX-LKL   ]  MRENCLAVE: 4e01b6e3224885594a8571767375ff4d7a1fea9ef55262177a2c2b80137ef58f
[    SGX-LKL   ]  MRSIGNER:  6e51044645648a1da224202a4b542666c5425b9e8614667efd22662f0cb4e845
[    SGX-LKL   ] Verification of quote and attestation report successful.
[    SGX-LKL   ] Enclave report data:
[    SGX-LKL   ]   Nonce: 0
[    SGX-LKL   ]   Public wireguard key: U6eJFenuwhClipTAWoPyNbqlMLTsy66LfAbhmJYV5mA=
```

## Other tools
Feel free to use another tool of your choice
