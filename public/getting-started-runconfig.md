# Send run configuration

Now that we can ensure that we are really talking to our enclave and not some man-in-the-middle we can send our run configuration ( which might inherit sensitve information and secrets).
In our example we are not using senstive information but just the default run configuration. To startup the redis database use the burger menu and choose the option <code> send run configurration </code>.

A new dialog will open up which will ask you what to run. As we specified a default run configuration this be proposed per default. So you can just click the button below to start up redis in unbprotected mode on port 56789.
<br><br>

 ![Send run config](assets/img/ui_sendrunconfig.png)

<br><br>

 After you have sent the remote config you should see a message below that the configuration has been successfully sent and that the status switched to running which indicates the application is being executed. In advance you'll see that a random port has been assigned to the defined mapping on port 56789. While redis in-enclave is listening on port 56789 the securitee-paas maps this ports to a random port and ensures firewall rules are set so that you are able to access your application.

<br><br>

 ![Run configuration sent](assets/img/ui_remoteconfigsent.png)


Now that the application is running, let's check the console log to see what's going on
<br><br>

 ![Run configuration sent](assets/img/ui_applicationstarted.png)

 We can see that redis is up and running. That's it for starting an application in-enclave via the securitee-paas.
 