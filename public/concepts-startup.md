# Startup and intialization


#  Reading config & environment variables

When the securitee-paass starts up it searches for an init.cfg in which all options which are set can be found..

If running in non-persistent mode the init.cfg isn't existing at startup and will be created. In addition to that, environment variables are checked to verify if a username and password has been set to automatically authenticate the user and register the node.


##  Creating System selfdescription

Nodeinfo → HArdware und software capabilities | Selbstcheck on startup

Processors initialized → Processors → SGXPRocessor

  
  

SELBSTBESCHREIBUNG DIE VERÖFFENTLICH WIRD


### Setting up wireguard

![wireguard](https://www.wireguard.com/img/wireguard.svg)

WireGuard® is an extremely simple yet fast and modern VPN that utilizes state-of-the-art [cryptography](https://www.wireguard.com/protocol/ "https://www.wireguard.com/protocol/"). It aims to be [faster](https://www.wireguard.com/performance/ "https://www.wireguard.com/performance/"), [simpler](https://www.wireguard.com/quickstart/ "https://www.wireguard.com/quickstart/"), leaner, and more useful than IPsec, while avoiding the massive headache. It intends to be considerably more performant than OpenVPN. WireGuard is designed as a general purpose VPN for running on embedded interfaces and super computers alike, fit for many different circumstances. Initially released for the Linux kernel, it is now cross-platform (Windows, macOS, BSD, iOS, Android) and widely deployable. It is currently under heavy development, but already it might be regarded as the most secure, easiest to use, and simplest VPN solution in the industry.

WireGuard uses state-of-the-art cryptography, like the [Noise protocol framework](http://www.noiseprotocol.org/ "http://www.noiseprotocol.org/"), [Curve25519](http://cr.yp.to/ecdh.html "http://cr.yp.to/ecdh.html"), [ChaCha20](http://cr.yp.to/chacha.html "http://cr.yp.to/chacha.html"), [Poly1305](http://cr.yp.to/mac.html "http://cr.yp.to/mac.html"), [BLAKE2](https://blake2.net/ "https://blake2.net/"), [SipHash24](https://131002.net/siphash/ "https://131002.net/siphash/"), [HKDF](https://eprint.iacr.org/2010/264 "https://eprint.iacr.org/2010/264"), and secure trusted constructions. It makes conservative and reasonable choices and has been reviewed by cryptographers.

  
  

  
  


###  Setting up IPFS

![ipfs](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/IPFS_logo.png/800px-IPFS_logo.png)

IPFS is a distributed system for storing and accessing files, websites, applications, and data. 

IPFS is Making it possible to download a file from many locations that aren’t managed by one organization…

*   **Supports a resilient internet.** If someone attacks Wikipedia’s web servers or an engineer at Wikipedia makes a big mistake that causes their servers to catch fire, you can still get the same webpages from somewhere else.
    
*   **Makes it harder to censor content.** Because files on IPFS can come from many places, it’s harder for anyone (whether they’re states, corporations, or someone else) to block things. In 2017, Turkey blocked Wikipedia and Spain blocked access to sites related to the Catalonian independence movement. We hope IPFS can help provide ways to circumvent actions like these when they happen.
    
*   **Can speed up the web when you’re far away or disconnected.** If you can retrieve a file from someone nearby instead of hundreds or thousands of miles away, you can often get it faster. This is especially valuable if your community is networked locally, but doesn’t have a good connection to the wider internet. (Well-funded organizations with technical expertise do this today by using multiple data centers or CDNs — [content distribution networks](https://en.wikipedia.org/wiki/Content_delivery_network "https://en.wikipedia.org/wiki/Content_delivery_network"). IPFS hopes to make this possible for everyone.)
    

That last point is actually where IPFS gets its name: **Inter-Planetary File System**. THey'restriving to build a system that works across places as disconnected or as far apart as planets. While that’s an idealistic goal, it keeps us working and thinking hard, and most everything we create in pursuit of that goal is also useful here at home.

The IPFS project seeks to evolve the infrastructure of the Internet and the Web, with many things we've learned from successful systems, like [Git](https://git-scm.com/ "https://git-scm.com/"), [BitTorrent](http://bittorrent.org/ "http://bittorrent.org/"), [Kademlia](https://en.wikipedia.org/wiki/Kademlia "https://en.wikipedia.org/wiki/Kademlia"), [Bitcoin](https://bitcoin.org/ "https://bitcoin.org/"), and many, many more. This is the sort of thing that would have come out of ARPA/DARPA, IETF, or Bell Labs in another age. IPFS is a free, open-source project with thousands of contributors.

IPFS ([the InterPlanetary File System](https://docs-beta.ipfs.io/concepts/what-is-ipfs/ "https://docs-beta.ipfs.io/concepts/what-is-ipfs/")) is a hypermedia distribution protocol addressed by content and identities. It enables the creation of completely distributed applications, and in doing so aims to make the web faster, safer, and more open.

IPFS is a distributed file system that seeks to connect all computing devices with the same system of files. In some ways, this is similar to the original aims of the Web, but IPFS is actually more similar to a single BitTorrent swarm exchanging Git objects. You can read more about its origins in the paper [IPFS - Content Addressed, Versioned, P2P File System](https://github.com/ipfs/ipfs/blob/master/papers/ipfs-cap2pfs/ipfs-p2p-file-system.pdf?raw=true "https://github.com/ipfs/ipfs/blob/master/papers/ipfs-cap2pfs/ipfs-p2p-file-system.pdf?raw=true").

  
  

#### Creating a keypair / peer identity


#### Starting up IPFS Node


  
  

  
  

The system is ready to be used when you can see the following message


  
  

* * *

  
  

  
  

Authentication
--------------

Einmalig nach start Node claimen → [http://cluster.securitee.tech:9291/ui/login](http://cluster.securitee.tech:9291/ui/login "http://cluster.securitee.tech:9291/ui/login")

Weiterleitung startseite:


  
  

Wird für den Node Schlüsselpaar erstellt → SECURITEE Schlüsselpaar

Java Keystore → Passwortgeschützte verschlüsselte datei zur speicherung von passwörtern und zertifikaten ( Public & Private key )

  
  

  
  

* * *

General usage
-------------

  
  


  
  


### Configure the node


  
  

To be able to use the Intel Attestation Service, you will have to register with Intel first. You can do this [here](https://software.intel.com/en-us/form/sgx-onboarding "https://software.intel.com/en-us/form/sgx-onboarding"). You will be assigned a Service Provider ID (SPID) as well as a subscription key. As part of the registration you also choose whether you require the verification of unlinkable or linkable quotes (see [Signature Policy](https://software.intel.com/en-us/articles/signature-policy "https://software.intel.com/en-us/articles/signature-policy")). The SPID, the subscription key, and the IAS signing CA certificate can then be used together to request an IAS attestation report and verify it.

After Signing up for the subscriptions it should look like this:

SGXLKL\_IAS\_SPID: Specifies the Service Provider ID (SPID) required for communication with the Intel Attestation Service (IAS).

SGXLKL\_IAS\_QUOTE\_TYPE: Specifies the quote type: '0' for unlinkable quotes (default), '1' for linkable quotes.

SGXLKL\_IAS\_SUBSCRIPTION\_KEY: IAS subscription key.

SGXLKL\_IAS\_SERVER: IAS server to use (Default: test-as.sgx.trustedservices.intel.com:443).

  
  

### Checking System information

* * *

  
  

Environments
------------

  
  

  
  

### Creating an environment

  
  

To run applications within enclaves  they need to be provided as  enviroment which isis an disk image. The securitee-paass manages the disk handling via dm and SGX-LKL.

[A](https://en.wikipedia.org/wiki/A "https://en.wikipedia.org/wiki/A") **disk image**, in computing, is a [computer file](https://en.wikipedia.org/wiki/Computer_file "https://en.wikipedia.org/wiki/Computer_file") containing the contents and structure of a disk [volume](https://en.wikipedia.org/wiki/Volume_(computing) "https://en.wikipedia.org/wiki/Volume_(computing)") or of an entire [data storage](https://en.wikipedia.org/wiki/Data_storage "https://en.wikipedia.org/wiki/Data_storage") device, such as a [hard disk drive](https://en.wikipedia.org/wiki/Hard_disk_drive "https://en.wikipedia.org/wiki/Hard_disk_drive"), [tape drive](https://en.wikipedia.org/wiki/Tape_drive "https://en.wikipedia.org/wiki/Tape_drive"), [floppy disk](https://en.wikipedia.org/wiki/Floppy_disk "https://en.wikipedia.org/wiki/Floppy_disk"), [optical disc](https://en.wikipedia.org/wiki/Optical_disc "https://en.wikipedia.org/wiki/Optical_disc"), or [USB flash drive](https://en.wikipedia.org/wiki/USB_flash_drive "https://en.wikipedia.org/wiki/USB_flash_drive"). A disk image is usually made by creating a [sector](https://en.wikipedia.org/wiki/Disk_sector "https://en.wikipedia.org/wiki/Disk_sector")\-by-sector copy of the source medium, thereby perfectly replicating the structure and contents of a storage device independent of the [file system](https://en.wikipedia.org/wiki/File_system "https://en.wikipedia.org/wiki/File_system"). Depending on the disk image format, a disk image may span one or more computer files.Another common use is to provide virtual disk drive space to be used by [SystemVirtualization](https://wiki.debian.org/SystemVirtualization "https://wiki.debian.org/SystemVirtualization"). This can prevent the CD from getting burned or damaged. It can also reduce bulk when one wishes to carry the contents of the CD along with oneself: one can store disk images to a relatively lightweight and bootable storage device which has a higher storage capacity than that of a CD (i.e. a USB keydrive).

  
  

Since SGX-LKL is built on top of musl, applications are expected to be dynamically linked against musl. musl and glibc are not fully binary-compatible. Applications linked against glibc are therefore not guaranteed to work with SGX-LKL. The simplest way to run an application with SGX-LKL is to use prebuilt binaries for Alpine Linux which uses musl as its C standard library.

  
  

#### Specifiying a name

#### Set the image size

#### Alpine Packages to be installted

**Software packages for Alpine Linux** are digitally signed tar.gz archives containing programs, configuration files, and dependency metadata. They have the extension .apk, and are often called "a-packs". As new user you can read the [Alpine newbie apk packages](https://wiki.alpinelinux.org/wiki/Alpine_newbie_apk_packages "https://wiki.alpinelinux.org/wiki/Alpine_newbie_apk_packages") page.

The programs, the **software installed to Alpine comes from two places: repository** with the following structure: http://<host>/alpine/<version>/<brach> (an URl that can be invoked with [apk](https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management "https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management") listed in the /etc/apk/repositories file) **and original upstream sources** (those compiled as Unix-like traditional way).

Are managed with the [apk](https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management "https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management") command, located at /sbin/apk, it uses /etc/apk/ place for the configurations files, and stores all downloaded "a-packs" files in /etc/apk/cache from the repositories before unpacks and put the package files compiled into the installed system.

Alpine software repositories have main packages and contributions made:

*   **About the main packages**: Main packages are the Alpine package software that have direct support and updates from the Alpine core and main team, also have official special documentation. Are always available for all releases and will have almost substitutions if some are not continued from upstream. Commonly those packages are selected due their responsibility and stability respect upstream availability.
    
*   **About the contribution ones**: User package contribution repositories are those made by users in team with the official developers and well near integrated to the Alpine packages. Those have supported by those user contributions and could end if the user also ends respect with Alpine work, by example could not have substitution in next release due lack of support by upstream author.
    

  
  

Only the main repository is enabled per default, so apk will not include packages from the other repositories.   
  

  
  

[https://pkgs.alpinelinux.org/packages](https://pkgs.alpinelinux.org/packages "https://pkgs.alpinelinux.org/packages")

### Publishing an environment

### Deleting  an environment

  
  

* * *

  
  

Processing
----------

  
  

  
  

### Booting an enclave

### Configure and run enclave

### Kill enclave

  
  

How to python in secure enviornment video:
------------------------------------------

  
  

  
  

Gefällt mirSeien Sie der Erste, dem dies gefällt.

Keine Labels

Schreiben Sie einen Kommentar...
