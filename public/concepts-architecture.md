# Architecture
Securitee PaaS uses a fork of the Linux Kernel Library to handle enclave management . Our architecture combines mature implementation with simple and easy maintenance.
 
![System architecture](assets/img/systemarchitecture.png)

Securitee PaaS is based on Linux’s ‘no MMU architecture’ and enables full in-enclave filesystem support as well as access to the full networking stack. In turn, this provides a much wider usage as it allows the user to natively run applications without the need to adjust them for in-clave usage. This is a main advantage in comparison to other solutions that provide support for Intel SGX.

Compared to other solutions, which only provide static linking, Securitee PaaS provides in-enclave, integrity protected dynamic linking. This allows a wide range of applications to be run without modifications. Every dynamically linked program contains a small statically linked function that is called once the program is started. This static function only maps the link library into memory and runs the code that the function contains. The link library determines all the dynamic libraries, which the program re-quires along with the names of the variables and functions from the librar-ies. It does so by reading the information contained in sections of the li-brary.

![Dynamic Linking](assets/img/dynamiclinking.png)

Dynamically linked libraries are mapped to the Processor Reserved Memory and resolved so that the references to the symbols contained in those libraries are accessible only within the enclave. Direct memory access targeting the PRM is rejected by the CPU in order to protect the enclave from other peripherals.
