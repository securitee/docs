Troubleshooting
===============

Erstellt von


\[ SGX-LKL \] Error: Error: lkl\_mount\_blockdev()=Invalid argument (-22)

This means there is an error with your disk configuration in the remote config file, most probably you have supplied the wrong disk hash

\[ SGX-LKL \] Warning: Failed to open AESM socket /var/run/aesmd/aesm.socket: No such file or directory  
\[ SGX-LKL \] Warning: Failed to initialize quote. Remote attestation will not be possible.

The Intel aesmd service cannot be reached / is not started, thus production mode and generating a quote ( and remote attestattion ) is not possible.

sgx-lkl-run: attestation/aesm.c:112: init\_quote\_response: Assertion \`qr->has\_targetinfo' failed.

Occurs when no valid SGX\_IAS\_SPID is defined and production execution is specified

Failed to open SGX device:  
/dev/isgx: No such file or directory  
/dev/sgx: No such file or directory

No SGX device is available. The relating Intel SGX drivers are not installed / no compatible hardware can be found.

Could not reserve enough space for 45056KB object heap\`\`\`

You are most probably running a java application but have not specified the extra environment options for java applications. For running java applications add these environement variables: SGXLKL\_HEAP=256M SGXLKL\_KEY=./build/config/enclave\_debug.key SGXLKL\_MMAP\_FILES=Shared SGXLKL\_STHREADS=4 SGXLKL\_ETHREADS=4

\[ SGX-LKL \] Error: Tap device sgxlkl\_tap0 unavailable, ioctl("/dev/net/tun"), TUNSETIFF) failed: Device or resource busy

If securitee-paas is running in standalone or compose mode only one TUN/TAP device is available resulting in only one enclave at time being able to startup. This message means that device is already used by another enclave thus resulting in an error not being able to startup the new enclave

Gefällt mirSeien Sie der Erste, dem dies gefällt.

Keine Labels

Schreiben Sie einen Kommentar...