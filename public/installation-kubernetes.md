# Kubernetes

Make sure you have kubernetes installed on your server and on your client a valid kube-congif to access your cluster. In case you are not familiar with kubernetes you cann follow this quickstart guide using microk8s.

## Microk8s quickstart guide

### Install microk8s | kubernetes on Host
```
snap install microk8s --classic
```
Get kubectl configuration and save it to ~/.kube/config on local machine

### microk8s.config

Enable necessary services
```
microk8s.enable dns ingress rbac storage
```
Install tiller and cert-manager and prometheus via kubernetes GUI


## Instal AESMD Daemonset

For each node in the kubernetes cluster a aesmd daemonset needs to be deployed
```
wget https://raw.githubusercontent.com/SECURITEE/sgx-aesmd/master/aesmd-daemonset.yaml
microk8s.kubectl apply -f aesmd-daemonset.yaml

```
## Creating volumes
For persistence of data the chart which will be deployed in the next step, 2 persistent volume claims will be created. 

```
NAME                                STATUS   VOLUME    CAPACITY   ACCESS MODES   STORAGECLASS        AGE
chart-1597343192-securitee-paas-ipfs                                  RWO            microk8s-hostpath   12m
chart-1597343192-securitee-paas-node                                  RWO            microk8s-hostpath   12m

```

To satisfy the pvc create two volumes which will be used for the ipfs and securitee-paas pod.
The storageclass must equal "microk8s-hostpath" . For testing purposes you can create 2 hostpath pv.

Create the destination folders:
```
sudo mkdir /volumes
sudo mkdir /volumes/volume1
sudo mkdir /volumes/volume2

```

Create a new file called volume1.yaml
```
kind: PersistentVolume
apiVersion: v1
metadata:
  name: volume1
  labels:
    type: local
spec:
  capacity:
    storage: 20Gi
  storageClassName: microk8s-hostpath
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/volumes/volume1"

```
Deploy it
```
microk8s.kubectl apply -f volume1.yml

```

Create a new file called volume2.yaml
```
kind: PersistentVolume
apiVersion: v1
metadata:
  name: volume2
  labels:
    type: local
spec:
  capacity:
    storage: 20Gi
  storageClassName: microk8s-hostpath
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/volumes/volume2"

```
Deploy it
```
microk8s.kubectl apply -f volume2.yml

```

Check if the volumes have been created
```
microk8s.kubectl get pv


NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                                       STORAGECLASS        REASON   AGE
volume1   20Gi       RWO            Retain           Bound    default/chart-1597343192-securitee-paas-node   microk8s-hostpath            12m
volume2   20Gi       RWO            Retain           Bound    default/chart-1597343192-securitee-paas-ipfs   microk8s-hostpath            12m

```



## Deploy chart

For each node in the kubernetes cluster a aesmd daemonset needs to be deployed
```
git clone https://gitlab.com/securitee/securitee-paas.git
helm install ./chart --generate-name

```