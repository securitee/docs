![logo](assets/img/logo.png)

# securitee-paas

The securitee-paas enables users to secure applications and data during runtime. Our solution provides secure enclaves to protect data, communication, algorithms and IP, thereby increasing the IT system’s robustness against malicious attacks.

- Simple and lightweight
- No statically built html files
- Multiple themes

[GitLab](https://gitlab.com/securitee)
[Getting Started](?id=concept)

![color](#f3f3f6)
