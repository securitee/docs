# Enable Intel SGX support
If you don't want to run the securitee-paas within secure hardware mode you can skip this section.


## Bios

Enable sgx in bios [one time only]


## Check kernel headers

To build the intel sgx driver, the version of installed kernel headers must match the active kernel version on the system.  To check if matching kernel headers are installed:
```
$ dpkg-query -s linux-headers-$(uname -r)
```

To install matching headers:
```
$ sudo apt-get install linux-headers-$(uname -r)
```

## Compile and install sgx module for kernel

 

The sgx kernel module needs to be recompiled for each kernel update
```
sudo apt install make gcc -y
git clone https://github.com/intel/linux-sgx-driver.git
cd linux-sgx-driver/
make
sudo mkdir -p "/lib/modules/"`uname -r`"/kernel/drivers/intel/sgx"
sudo cp isgx.ko "/lib/modules/"`uname -r`"/kernel/drivers/intel/sgx"
sudo sh -c "cat /etc/modules | grep -Fxq isgx || echo isgx >> /etc/modules"
sudo /sbin/depmod
sudo /sbin/modprobe isgx
```
## Download sgx driver and install it

For the latest drivers please refer to https://01.org/intel-software-guard-extensions/downloads
```
wget https://download.01.org/intel-sgx/sgx-linux/2.7.1/distro/ubuntu18.04-server/sgx_linux_x64_driver_2.6.0_4f5bb63.bin

sudo -i
chmod 0764 sgx_linux_x64_driver_2.6.0_4f5bb63.bin
exec ./sgx_linux_x64_driver_*.bin
```

