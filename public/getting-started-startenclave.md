# Start enclave

Now as we have prepared our environment we are ready to startup our enclave. In general the startup process can be split up in three different steps.

1. The enclave is initialized
2. The environment is booted and the integrity is validated
3. Configuration is used to start application (and optionally decrypt the image)

## Initalize enclave
To initialize the enclave and boot up the environment you only have to to choose the <code>Start {Environmentname}</code> in the burgermenu of the desired environment. A new dialog will open up which let's you decide if you want to run in simulation or production mode. While the production mode requires an Intel SGX compatible CPU and is the only one who allows remote attestation, the software simulation mode can be used to initialize the enclave on any Intel CPU without hardware security guarantees. 

As we want to validate that we are really running our application in-enclave which requires remote attestation, we choose production mode. Optionally we can specify ports that should be forwarded to the enclave. We always have the option to connect to the enclave via an internal wireguard connection but some applications might need to be diretcly accessable from the internet. In our getting started guide we will directly access an redis database which, as we specified in the defaul run configuration when creating the environment, will be listening on port 56789. To be able to access it directly we add the port and hit run environment which leads us to the processing page.
<br><br>

 ![Environment page](assets/img/ui_startenclave.gif)

On the processing page we can see all running enclaves and enclaves that have been previously started. Each enclave has a status that indicates if the enclave has been initialized, failed, or is running. Upon succesful startup the enclave should switch to the status <code>Started</code> which indicates that the enclave has been successfully initialized.

Upon initialisation the securitee-paas automatically validates the integrity of the environment and starts a remote attestation. Details can be inspected by opening the logs of the enclave on the burgermenu.
<br><br>

 ![Environment page](assets/img/ui_processingenclavelog.gif)

A sample enclave log looks like this:

```
[    SGX-LKL   ] Maximum enclave threads (TCS): 8
[    SGX-LKL   ] Kernel command line: ""
[    SGX-LKL   ] Adding entropy to entropy pool.
[    SGX-LKL   ] wg0 has public key JmodQZY0k7k/vBgxZt2LZXwvriy1qlO6gghaHLw0fVA=
[    SGX-LKL   ] Enclave report nonce: 0
[    SGX-LKL   ] Received quote from launch enclave:
[    SGX-LKL   ]  MRENCLAVE: 4e01b6e3224885594a8571767375ff4d7a1fea9ef55262177a2c2b80137ef58f
[    SGX-LKL   ]  MRSIGNER:  6e51044645648a1da224202a4b542666c5425b9e8614667efd22662f0cb4e845
[    SGX-LKL   ] Sending IAS request...
[    SGX-LKL   ] Intel Attestation Service Response:
[    SGX-LKL   ]  Quote status: GROUP_OUT_OF_DATE
[    SGX-LKL   ]  EPID group flags: 0x4
[    SGX-LKL   ]  TCB evaluation flags: 0x0f
[    SGX-LKL   ]  PSE evaluation flags: 0x00
[    SGX-LKL   ] Starting attestation server, listening on 192.168.100.1:56000...
[    SGX-LKL   ] Starting remote control server, listening on 192.168.100.1:56001...
[    SGX-LKL   ] Waiting for application run request...
```
The intitialisation outputs the public key of the enclave which we can later on use to validate if we are connection to the right enclave when using the built-in wireguard connection.
Afterwards we see an output of the MRENCLAVE and MRSIGNER which are used for the remote attesation. We'll get into this in more details in the next section. Afterwards we are doing an remote attesation which comes back
with the result GROUP_OUT_OF_DATE which means that that the enclave could successfully be remote attestated but that there is a new firmware / driver .

As last steps an remote attesation server and remote control server are intialized. The attestation server can be used to generate a quote and execute a remote attestation from a remote host and the remote control server can be used to send the run configuration from a remote host.

But before we send over some senstive information to an application we want to make sure that this is really our application running within the enclave.

