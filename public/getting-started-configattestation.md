# Configure remote attestation
In this getting started guide we will use remote attestation to validate that we are really running our desired payload in-enclave. For mor information on the general remote attestation process please refer to the [Remote attestation process]()


  
## Create an Intel developer account

To be able to use the Intel Attestation Service, you will have to register with Intel first. You can do this [here](https://software.intel.com/en-us/form/sgx-onboarding "https://software.intel.com/en-us/form/sgx-onboarding"). You will be assigned a Service Provider ID (SPID) as well as a subscription key. As part of the registration you also choose whether you require the verification of unlinkable or linkable quotes (see [Signature Policy](https://software.intel.com/en-us/articles/signature-policy "https://software.intel.com/en-us/articles/signature-policy")). The SPID, the subscription key, and the IAS signing CA certificate can then be used together to request an IAS attestation report and verify it.
<br><br>

![intel developers](assets/img/ui_intelsetup.png)

You should now see your subscriptions on [https://api.portal.trustedservices.intel.com/developer](https://api.portal.trustedservices.intel.com/developer)

## Configure the securitee-paas
In order to use your created subscription you'll need to visit the settings page on /ui/settings and scroll a little bit down to the additional setttings section.
You will now have to set the following values based in the information from the subscription base.
<br><br>

**SGXLKL\_IAS\_SPID** - Specifies the Service Provider ID (SPID) required for communication with the Intel Attestation Service (IAS).

**SGXLKL\_IAS\_QUOTE\_TYPE** - Specifies the quote type ( '0' for unlinkable quotes (default), '1' for linkable quotes=.

**SGXLKL\_IAS\_SUBSCRIPTION\_KEY** - IAS subscription key (Subscription primary key).

<br><br>
 The values are automatically saved after you are pressing enter in the related textfield.

![Remote attestation config](assets/img/ui_remoteattestationconfig.png)
<br><br>

That's it! 

We are to let our applications remote attested by Intel.
  
  