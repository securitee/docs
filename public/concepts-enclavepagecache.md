# Enclave Page Cache
The SGX design supports having multiple enclaves on a system at the same time. Thus, it fulfils an important necessity in multi-process environ-ments. Having multiple enclaves on a system at the same time is achieved through an Enclave Page Cache (EPC) split into 4 KB pages that can be as-signed to different enclaves. The EPC uses the same page size as the architecture’s address translation feature.

![Enclave Page Cache](assets/img/epc.png)

The EPC is managed by the same system software that manages the rest of the computer’s physical memory. The system software, which can be a hypervisor or an OS kernel, uses SGX instructions to allocate unused pages to enclaves, and to free up previously allocated EPC pages. Non-enclave software cannot directly access the EPC, since it is contained in the PRM.

