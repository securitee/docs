# Environment integrity protection
Securitee PaaS supports the verified boot of environments via the option-al kernel function ‘Device-Mapper-Verity’ (dm-Verity), which enables trans-parent integrity checks of block devices. ‘Dm-verity’ prevents permanent rootkits that can retain root privileges and compromise devices. With this feature, users can be sure that a device is in the same state while booting as when it was last used or created.

<img align="right" src="assets/img/hashtree.png" height="400px">
Malicious applications with enough privileges on the untrusted host could try to hide from detection programs and alter environments leading to ma-licious code being executed within the enclave. This, in turn would expose an application and put data confidentiality at substantial risk. 

The ‘dm-verity’ feature enables the user to view a block device, the under-lying storage level of the file system, and allows the software to determine whether the device matches the expected configuration. This is conduct-ed with a cryptographic hash tree, known as the Merkle tree (Figure 12). For each block (usually with a size of 4 KB) there is one SHA256 hash.
 
Figure 12: Example of a Cryptographic Hash Tree
Since the hash values are stored in a page tree, only the top level ‘root’ hash needs to be trusted to verify the rest of the tree. The ability to change one of the blocks is equivalent to resolving the cryptographic hash. Figure 12 above illustrates such a page tree structure.

